package com.ideafyapp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import com.ideafyapp.presentation.view.main.MainActivity

class TransitionExample : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transition_example)
        val view = View(this)

        //Esto se hace en el onBind del adaptador con el itemView
        view.setOnClickListener {
            val intent = Intent(view.context, MainActivity::class.java)

            //Enviar los datos para la vista
            intent.putExtra("title", "Hola")

            //Hacerlo para cada vista.. TextVIew, ImageVIew
            val pair = Pair.create(view, "transitionName")

            //Agregar todos los pairs para todas las vistas que requieran la animación
            val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(view.context as Activity, pair)
            view.context.startActivity(intent, optionsCompat.toBundle())


            //Recibir del otro lado con

            intent?.extras?.let {
                //setear las vistas
            }


        }

    }
}
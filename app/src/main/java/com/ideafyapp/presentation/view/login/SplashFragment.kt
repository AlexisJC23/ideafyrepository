package com.ideafyapp.presentation.view.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import com.ideafyapp.R
import com.ideafyapp.presentation.view.main.MainActivityDirections
import kotlinx.android.synthetic.main.fragment_splash.*


class SplashFragment : Fragment() {

    private lateinit var activityDirections: MainActivityDirections

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivityDirections) {
            activityDirections = context
        } else {
            throw RuntimeException("Context Must Implement MainActivityDirections")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivLogoIdeafy.startAnimation(AnimationUtils.loadAnimation(context, R.anim.anim).apply {
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation?) {
                    activityDirections.showLogin()
                }

                override fun onAnimationRepeat(animation: Animation?) {

                }

            })
        })
    }


}
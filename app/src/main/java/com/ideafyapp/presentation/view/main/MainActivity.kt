 package com.ideafyapp.presentation.view.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.ideafyapp.R

class MainActivity : AppCompatActivity(), MainActivityDirections {

    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navController = findNavController(R.id.fcontainer)
    }

    override fun showLogin() {
        navController.navigate(R.id.action_splashFragment_to_loginFragment)
    }

    override fun showIdeafyMain() {
        TODO("Not yet implemented")
    }

    override fun showSignUp() {
        navController.navigate(R.id.action_loginFragment_to_signUpFragment)
    }

    override fun showLoginFromSignUp() {
        navController.navigate(R.id.action_signUpFragment_to_loginFragment)
    }
}
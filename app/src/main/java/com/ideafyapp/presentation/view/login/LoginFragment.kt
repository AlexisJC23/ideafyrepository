package com.ideafyapp.presentation.view.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ideafyapp.BuildConfig
import com.ideafyapp.R
import com.ideafyapp.presentation.view.main.MainActivityDirections
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment() {

    private lateinit var activityDirections: MainActivityDirections

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivityDirections)
            activityDirections = context
        else
            throw RuntimeException("Fragment Must Implement MainActivityDirections")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (BuildConfig.DEBUG) {
            etUserEmail.setText("Debug")
            etUserPassword.setText("123")
        }

        btnLogin.setOnClickListener {
            if (etUserEmail.text.toString() == "Debug" && etUserPassword.text.toString() == "123") {

            } else
                Toast.makeText(
                    requireContext(),
                    "Contraseña o correo incorrectos",
                    Toast.LENGTH_SHORT
                ).show()


        }

        btnCreateAccount.setOnClickListener {
            activityDirections.showSignUp()
        }

    }


}
package com.ideafyapp.presentation.view.login

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.ideafyapp.R
import com.ideafyapp.presentation.view.main.MainActivityDirections
import kotlinx.android.synthetic.main.fragment_signup.*

class SignUpFragment : Fragment() {

    private lateinit var activityDirections: MainActivityDirections

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivityDirections)
            activityDirections = context
        else
            throw RuntimeException("Fragment must implement MainActivityDirections")

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_signup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCancel.setOnClickListener {
            Toast.makeText(requireContext(), "Navegaré al LoginFragment", Toast.LENGTH_SHORT).show()
            activityDirections.showLoginFromSignUp()
        }
    }

}
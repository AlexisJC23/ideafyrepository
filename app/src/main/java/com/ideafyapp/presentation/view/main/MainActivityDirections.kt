package com.ideafyapp.presentation.view.main

interface MainActivityDirections {
    fun showLogin()
    fun showIdeafyMain()
    fun showSignUp()
    fun showLoginFromSignUp()
}